export default function GeneralInformation() {
    return (
        <div>
            <div className="px-32 font-montserrat">
                <h2 className="font-extrabold text-xl">Informasi Umum</h2>
                <p className="text-gray-500 text-base">
                    Zoglo membantu kamu untuk memiliki rumah di lokasi terbaik, Bantu
                    kami untuk memahami keinginan kamu dan mendirikan rumah impian
                    kamu.
                </p>
                <div className="my-6">
                    <p className="text-gray-500 text-sm">Nama panggilan kamu siapa?</p>
                    <input type="text"
                           className="border-b-2 border-gray-500 focus:outline-none py-1 font-bold"/>
                </div>
            </div>
        </div>
    )
}