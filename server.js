/* eslint-disable no-undef */
const express = require('express');
const next = require('next');

const dev = process.env.NODE_ENV !== 'production';
console.log('dev', dev);
const nextApp = next({
  dev,
});
const handle = nextApp.getRequestHandler();
nextApp
  .prepare()
  .then(() => {
    const server = express();
    const port = 8000;

    server.get('*', (req, res) => {
      return handle(req, res);
    });
    server.listen(port, (err) => {
      if (err) throw err;
      console.log(`>> Ready on  ${port}`);
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
