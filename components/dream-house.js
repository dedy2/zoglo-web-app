export default function DreamHouse() {
    return (
        <div>
            <div className="pl-32 pr-64 font-montserrat">
                <h2 className="font-extrabold text-xl">Rumah Impian</h2>
                <p className="text-gray-500 text-base">
                    Zoglo membantu kamu untuk memiliki rumah di lokasi terbaik, Bantu kami untuk memahami keinginan kamu
                    dan mendirikan rumah impian kamu.
                </p>
                <div className="my-6">
                    <h2 className="text-black text-lg font-bold my-4">Hai Rahmah, salam kenal.
                        Kamu pengen punya rumah seperti apa?</h2>
                    <div className="my-4">
                        <label htmlFor="lokasi" className="text-gray-500 text-xs">Lokasi</label>
                        <select name="lokasi" id="lokasi"
                                className="border-b-2 border-gray-500 focus:outline-none py-1 font-bold w-full">
                            <option value="Bekasi">Bekasi</option>
                            <option value="Jakarta">Jakarta</option>
                            <option value="Tanggerang">Tanggerang</option>
                            <option value="Bogor">Bogor</option>
                            <option value="Depok">Depok</option>
                        </select>
                    </div>
                    <h2 className="text-black text-lg font-bold mb-2">Harga</h2>
                    <div className="grid grid-cols-2 gap-2 mb-4">
                        <div>
                            <label htmlFor="lokasi" className="text-gray-500 text-xs">dari</label>
                            <input type="text"
                                   className="border-b-2 border-gray-500 focus:outline-none py-1 font-bold w-full"/>
                        </div>
                        <div>
                            <label htmlFor="lokasi" className="text-gray-500 text-xs">sampai</label>
                            <input type="text"
                                   className="border-b-2 border-gray-500 focus:outline-none py-1 font-bold w-full"/>
                        </div>
                    </div>
                    <h2 className="text-black text-lg font-bold mb-2">Cicilan per bulan</h2>
                    <div className="grid grid-cols-2 gap-2 mb-4">
                        <div>
                            <label htmlFor="lokasi" className="text-gray-500 text-xs">dari</label>
                            <input type="text"
                                   className="border-b-2 border-gray-500 focus:outline-none py-1 font-bold w-full"/>
                        </div>
                        <div>
                            <label htmlFor="lokasi" className="text-gray-500 text-xs">sampai</label>
                            <input type="text"
                                   className="border-b-2 border-gray-500 focus:outline-none py-1 font-bold w-full"/>
                        </div>
                    </div>
                    <div className="mb-4">
                        <h2 className="text-black text-lg font-bold mb-2">Akses transportasi</h2>
                        <div className="grid grid-cols-3 gap-2">
                            <div
                                className="border-blue-500 text-blue-500 inline-flex items-center font-bold text-base border-2 rounded px-4 py-2">
                                <input type="radio" className="mr-3"/>
                                KRL
                            </div>
                            <div
                                className="border-blue-500 text-blue-500 inline-flex items-center font-bold text-base border-2 rounded px-4 py-2">
                                <input type="radio" className="mr-3"/>
                                Bus
                            </div>
                            <div
                                className="border-blue-500 text-blue-500 inline-flex items-center font-bold text-base border-2 rounded px-4 py-2">
                                <input type="radio" className="mr-3"/>
                                Kendaraan Pribadi
                            </div>
                        </div>
                    </div>
                    <div>
                        <label className="text-gray-500 text-xs">Lainnya</label>
                        <input type="text"
                               className="border-b-2 border-gray-500 focus:outline-none py-1 font-bold w-full"/>
                    </div>
                </div>
            </div>
        </div>
    );
}