import Head from "next/head";
import Router from "next/router";
import {
  IoLogoInstagram,
  IoLogoDribbble,
  IoLogoTwitter,
  IoLogoYoutube,
} from "react-icons/io";
import { useRouter } from "next/router";

export default function Home() {
  const router = useRouter();
  return (
    <div className="bg-transparent">
      <Head>
        <title>Zoglo App</title>
      </Head>
      <img
        draggable="false"
        src="/images/purple-circle.png"
        className="absolute right-0 top-0 -z-999999"
      />
      <header className="z-40 py-6 px-32 items-center justify-between">
        <div className="flex items-center justify-between">
          <div className="flex items-center block">
            <a href="#" className="block text-black font-bold text-base mx-6">
              Intro
            </a>
            <a
              onClick={() => router.push("recommendation")}
              className="block text-black font-bold text-base mx-6"
            >
              Recommended
            </a>
            <a href="#" className="block text-black font-bold text-base mx-6">
              Share
            </a>
          </div>
          <div className="flex items-center justify-center">
            <a href="#" className="text-md font-extrabold text-black">
              <img src="/images/Logo.png" className="h-6 w-6 mx-2 inline" />
              Zoglo
            </a>
          </div>
          <div className="flex items-center justify-between">
            <div className="border rounded-full p-2 mx-2">
              <IoLogoInstagram className="h-6 w-6" color="#FFF" />
            </div>
            <div className="border rounded-full p-2 mx-2">
              <IoLogoDribbble className="h-6 w-6" color="#FFF" />
            </div>
            <div className="border rounded-full p-2 mx-2">
              <IoLogoTwitter className="h-6 w-6" color="#FFF" />
            </div>
            <div className="border rounded-full p-2 mx-2">
              <IoLogoYoutube className="h-6 w-6" color="#FFF" />
            </div>
          </div>
        </div>
      </header>
      <main className="flex items-center justify-between p-40">
        <div className="max-w-4xl">
          <p className="text-6xl font-extrabold">
            Halo Sahabat Milenial! Selamat datang di Zoglo.
          </p>
          <p className="text-xl">
            Zoglo membantu kamu mendapatkan rumah impian dengan lokasi, harga
            dan investasi terbaik. Dapatkan pengalaman baru dalam membeli rumah
            bersama Zoglo.
          </p>
        </div>
        <div className="max-w-xl z-40">
          <img src="/images/image-banner.png" />
        </div>
      </main>
      <section className="px-40 py-10 bg-turquoise-100">
        <div className="mx-10 relative my-16 bg-turquoise-base rounded-2xl p-10 h-banner">
          <div className="grid grid-cols-2">
            <div className="align-bottom">
              <p className="text-lg text-black mb-5">
                Zoglo membantu kamu untuk memiliki rumah di lokasi terbaik,
                Bantu kami untuk memahami keinginan kamu dan mendirikan rumah
                impian kamu.
              </p>
              <button
                onClick={() => router.push("recommendation")}
                className="px-5 py-3 font-bold rounded-md bg-white text-purple-500 text-xl flex align-text-bottom"
              >
                Dapatkan rumah kamu sekarang
              </button>
            </div>
            <div>
              <img
                src="/images/bg-group-1.png"
                alt="Image 3"
                className="absolute top-0 right-0 rounded-r-2xl"
              />
            </div>
          </div>
        </div>
      </section>
      <section className="px-40 py-10">
        <h1 className="text-5xl text-black font-extrabold mb-10 max-w-2xl">
          Fitur Aplikasi Zoglo
        </h1>
        <div className="grid grid-cols-2 pl-16">
          <div>
            <div className="flex items-center my-16">
              <img className="mr-5" src="/images/feature-1.jpg" alt="Step 1" />
              <div>
                <h1 className="text-xl text-black">Headline</h1>
                <p className="text-base text-gray-800">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit
                </p>
              </div>
            </div>
            <div className="flex items-center my-16">
              <img className="mr-5" src="/images/feature-2.png" alt="Step 1" />
              <div>
                <h1 className="text-xl text-black">Headline</h1>
                <p className="text-base text-gray-800">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit
                </p>
              </div>
            </div>
            <div className="flex items-center my-16">
              <img className="mr-5" src="/images/feature-3.jpg" alt="Step 1" />
              <div>
                <h1 className="text-xl text-black">Headline</h1>
                <p className="text-base text-gray-800">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit
                </p>
              </div>
            </div>
            <div className="flex items-center my-16">
              <img className="mr-5" src="/images/feature-4.png" alt="Step 1" />
              <div>
                <h1>Headline</h1>
                <p className="text-base text-gray-800">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit
                </p>
              </div>
            </div>
          </div>
          <div>
            <img src="/images/phone-mockup.png" alt="Phone" />
          </div>
        </div>
      </section>
      <section className="bg-turquoise-base">
        <div className="grid grid-cols-2">
          <div className="px-40 py-10">
            <div className="mb-10">
              <h1 className="text-6xl font-extrabold text-black">
                Unduh Aplikasi Zoglo
              </h1>
              <p className="text-lg text-gray-900">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                odio in et, lectus sit lorem id integer. Lorem ipsum dolor sit
                amet, consectetur adipiscing elit. Nunc odio in et, lectus sit
                lorem id integer.
              </p>
            </div>
            <p className="text-lg text-gray-900 mb-2">Unduh Sekarang</p>
            <div className="inline-block mr-3">
              <img src="/images/play-store.png" alt="Play Store" />
            </div>
            <div className="inline-block">
              <img src="/images/app-store.png" alt="App Store" />
            </div>
          </div>
          <div>
            <img src="/images/phone-mockup-2.png" alt="Phone" />
          </div>
        </div>
      </section>
      <footer className="bg-black-100 text-center h-24 justify-center flex items-center">
        <span className="text-white">© Hak Cipta 2020. Zoglo.</span>
      </footer>
    </div>
  );
}
