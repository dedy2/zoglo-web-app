import Head from 'next/head'
import {useRouter} from 'next/router'

export default function RecommendedHouse() {
    const router = useRouter()
    return (
        <div className="items-center justify-center px-64 m-auto py-32 h-full font-montserrat-alternates nois">
            <Head>
                <title>Recommended House</title>
            </Head>
            <div className="bg-blue-500 p-16 rounded-lg">
                <h1 onClick={() => router.replace('/')} className="my-5 cursor-pointer align-middle text-center text-3xl text-white uppercase font-extrabold">Zoglo</h1>
                <h1 className="text-2xl mb-3 text-white font-extrabold">Hasil Rekomendasi Rumah</h1>
                <p className="text-base mb-2 text-white font-medium font-montserrat max-w-lg">Zoglo membantu kamu untuk
                    memiliki rumah di lokasi terbaik, Bantu kami untuk memahami keinginan kamu
                    dan mendirikan rumah impian kamu.</p>
                <div className="bg-white p-16 rounded-lg">
                    <h1 className="font-semibold text-black-100 text-base">Alamanda & Anthurium</h1>
                    <div className="grid grid-cols-2">
                        <div>
                            <h3 className="text-gray-500 text-sm mb-3">Type 27</h3>
                            <img src="/images/house.png" className="rounded-lg" alt=""/>
                        </div>
                        <div>
                            <img src="/images/sitemap.png" className="rounded-lg" alt=""/>
                        </div>
                    </div>
                </div>
                <div className="mt-5">
                    <p className="text-center font-medium text-2xl text-white">Bagikan ke temanmu dan dapatkan diskon pembayaran</p>
                    <div className="flex justify-center items-center gap-2">
                        <img src="/images/Instagram.png" alt="Instagram"/>
                        <img src="/images/Twitter.png" alt="Twitter"/>
                        <img src="/images/Facebook.png" alt="Facebook"/>
                        <img src="/images/Whatsapp.png" alt="Whatsapp"/>
                    </div>
                </div>
            </div>
            <style jsx>{`
                .nois {
                  background: linear-gradient(240.64deg, rgba(38, 150, 232, 0.15) 0%, rgba(38, 150, 232, 0.4) 100%);
                }
            `}</style>
        </div>
    )
}