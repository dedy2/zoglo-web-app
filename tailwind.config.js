module.exports = {
    future: {
        // removeDeprecatedGapUtilities: true,
        // purgeLayersByDefault: true,
    },
    purge: [],
    theme: {
        extend: {
            zIndex: {
                "-999999": "-999999",
            },
            spacing: {
                banner: "17.5rem",
            },
            colors: {
                turquoise: {
                    base: "#2EC5CE",
                    100: "#D5FAFC",
                },
                black: {
                    100: "#0B0D17",
                },
                "gradient-blue":
                    "linear-gradient(240.64deg, rgba(38, 150, 232, 0.15) 0%, rgba(38, 150, 232, 0.4) 100%)",
            },
            fontFamily: {
                "montserrat-alternates": ["Montserrat Alternates", "sans-serif"],
                "montserrat": ["Montserrat", "sans-serif"],
            },
        },
    },
    variants: {},
    plugins: [],
};
