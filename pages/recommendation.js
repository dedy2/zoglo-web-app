import React, {useState} from 'react'
import Head from "next/head";
import {useRouter} from 'next/router'
import GeneralInformation from "../components/general-information";
import DreamHouse from "../components/dream-house";
import ContactInformation from "../components/contact-informasion";

export default function Recommendation() {
    const [step, setStep] = useState(1)
    const [labelPrev, setLabelPrev] = useState("Kembali ke Homepage")
    const router = useRouter()

    const nextPage = () => {
        setStep(step + 1)
        switch (step + 1) {
            case 1:
                setLabelPrev("Kembali ke Homepage")
                break;
            case 2:
                setLabelPrev("Kembali ke Informasi Umum")
                break;
            case 3:
                setLabelPrev("Kembali ke Rencana Investasi")
                break;
        }
    }
    const prevPage = () => {
        if (step - 1 !== 0)
            setStep(step - 1)
        if (step - 1 === 0) {
            router.push("/")
        }
        switch (step - 1) {
            case 1:
                setLabelPrev("Kembali ke Homepage")
                break;
            case 2:
                setLabelPrev("Kembali ke Informasi Umum")
                break;
            case 3:
                setLabelPrev("Kembali ke Rencana Investasi")
                break;
        }
    }
    return (
        <div className="px-64 m-auto py-32 h-full font-montserrat-alternates nois">
            <Head>
                <title>Recommendation</title>
            </Head>
            <div className="px-20 flex">
                <div className="bg-blue-700 px-10 py-12 rounded-tl-lg rounded-bl-lg">
                    <h1 className="font-extrabold text-2xl text-white mb-48">Zoglo</h1>
                    <div className=" pb-48">
                        <div className="flex">
                            <div
                                className="w-5 h-5 border-2 rounded border-white items-center justify-center flex mr-2">
                                <div className="w-2 h-2 bg-white"/>
                            </div>
                            <h3 className="font-bold text-white">Informasi Umum</h3>
                        </div>
                        <div
                            className={`${step >= 1 ? 'vertical-dotted-line-active' : 'vertical-dotted-line'} ml-2 my-1`}/>
                        <div className="flex">
                            <div
                                className={`w-5 h-5 border-2 rounded ${step >= 2 ? 'border-white-300' : 'border-gray-300'} items-center justify-center flex mr-2`}>
                                {step >= 2 && <div className="w-2 h-2 bg-white"/>}
                            </div>
                            <h3 className="font-bold text-gray-300">Rumah Impian</h3>
                        </div>
                        <div
                            className={`${step > 2 ? 'vertical-dotted-line-active' : 'vertical-dotted-line'} ml-2 my-1`}/>
                        <div className="flex">
                            <div
                                className={`w-5 h-5 border-2 rounded ${step === 3 ? 'border-white-300' : 'border-gray-300'} items-center justify-center flex mr-2`}>
                                {step === 3 ? <div className="w-2 h-2 bg-white-300"/> : ''}
                            </div>
                            <h3 className="font-bold text-gray-300">Informasi Kontak</h3>
                        </div>
                    </div>
                </div>
                <div className="bg-white text-black-100 rounded-tr-lg rounded-br-lg w-3/4">
                    <div className="flex justify-between items-center py-12 px-32">
                        <h3 className="font-semibold text-lg text-gray-500">
                            Langkah ke {step} dari 3
                        </h3>
                        <h3 className="font-semibold inline-flex text-lg text-gray-500">
                            <span className="font-medium">Butuh bantuan?</span>{" "}
                            <span className="text-blue-700 mr-5 ">Hubungi Kami</span>
                            <img src="/images/arrow-left-blue.svg" className="transform rotate-180" alt=""/>
                        </h3>
                    </div>
                    {step === 1 && <GeneralInformation/>}
                    {step === 2 && <DreamHouse/>}
                    {step === 3 && <ContactInformation/>}
                    <div className="flex justify-between items-center py-12 px-32">
                        <div onClick={prevPage}>
                            <h3 className="font-semibold text-base inline-flex text-blue-500 cursor-pointer">
                                <img src="/images/arrow-left-blue.svg" className="mr-5" alt=""/>
                                {labelPrev}
                            </h3>
                        </div>
                        {step === 3 ? (
                            <div onClick={() => router.push("recommended-house")}
                                 className="font-semibold cursor-pointer text-base text-white inline-flex items-center bg-blue-500 px-4 py-2 rounded">
                                Dapatkan Rumah Sekarang
                                <img src="/images/arrow-right-white.svg" className="ml-5" alt=""/>
                            </div>
                        ) : (
                            <div onClick={nextPage}
                                 className="font-semibold cursor-pointer text-base text-white inline-flex items-center bg-blue-500 px-4 py-2 rounded">
                                Selanjutnya
                                <img src="/images/arrow-right-white.svg" className="ml-5" alt=""/>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <style jsx>{`
        .nois {
          background: linear-gradient(
            240.64deg,
            rgba(38, 150, 232, 0.15) 0%,
            rgba(38, 150, 232, 0.4) 100%
          );
        }
      `}</style>
        </div>
    );
}
